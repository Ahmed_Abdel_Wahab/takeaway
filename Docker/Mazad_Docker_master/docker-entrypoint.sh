#!/bin/bash
set -e

case $1 in
    master)
	    $JMETER_HOME/bin/jmeter-server \
		    -Dserver.rmi.ssl.disable=true \
		    -Dserver.rmi.localport=60000 \
		    -Djava.rmi.server.hostname=$hostip 
	    tail -f /dev/null
	    ;;
    server)
	    $JMETER_HOME/bin/jmeter-server \
	            -Dserver.rmi.ssl.disable=true
	    ;;
    *)
	    echo "Sorry, this option doesn't exist!"
	    ;;
esac

exec "$@"
