#!/bin/bash
set -e

case $1 in
    master)
        $JMETER_HOME/bin/jmeter-server \
	    -Dserver.rmi.ssl.disable=true
        tail -f /dev/null
        ;;
    server)
        $JMETER_HOME/bin/jmeter-server \
            -Dserver.rmi.localport=50000 \
            -Dserver_port=1099 \
            -Dserver.rmi.ssl.disable=true \
	-Djava.rmi.server.hostname=$hostip
	;;
    *)
        echo "Sorry, this option doesn't exist!"
        ;;
esac

exec "$@"
