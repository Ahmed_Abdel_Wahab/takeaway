# Takeaway
# Project Assessment:
Automated test toolkit for /albums, /users and inserting new record route to be triggered by: 
●	steps during continuous integration, 
●	steps during continuous delivery, 
●	Regular integrity checks of a running system. 
Project can be cloned from bitbucket using Git at: git clone https://Ahmed_Abdel_Wahab@bitbucket.org/Ahmed_Abdel_Wahab/takeaway.git 
Or even download from: https://bitbucket.org/Ahmed_Abdel_Wahab/takeaway
# Deliverables:
1.	Takeaway.jmx including all listed APIs 
2.	Readme_Description.docx
3.	Docker folder with docker master , slave images and also docker Jenkins setup
4.	Report folder for generated test 
5.	APDEX_ARCH to copy Report folder content to it after test exection
6.	apache-jmeter folder just in case you wanted to use it without downloading it.
# Target APIs:
1.	Get_HomePage
2.	Get_AlbumsAPI
3.	Get_PostsAPI
4.	Get_Posts?userIdAPI
5.	Get_UsersAPI
6.	Post_PostsAPI
7.	Get_todosAPI
8.	Get_commentsAPI
# Prerequisites:
1.	Install apache Jmeter over master machine which require Java +8 
‘Can be downloaded from : https://jmeter.apache.org/download_jmeter.cgi ’ , then .zip version for widnows , tgz for Unix based.
2.	Have docker installed on your master machine if you wish to auto-setup and configure environment using attached ‘DockerFile.yml , docker-compose.yml , docker-entrypoint.sh’ for full scalled testing over docker containers through pipleline command shell
# Notes:
1.	APIs automation is implemented using apache Jmeter. And execution sequence is parallel.  
2.	Assertions are included into every single API request ‘Response Code, Object Count’
3.	Aggregate report listener, summary, view results tree response time graph’ to check response time, throughput
•	You can manipulate Number of threads/user you wish to run by thread group properties or even directly through command line by overwriting following variables: -GThreads ‘in command line add –argument ‘variable’ ex: -GThreads=20
This will overwrite default value given into .jmx with given value over CL
•	-GRampUP ‘how much time you want all threads to become online in seconds’ ex: GRampUP =20
•	-GLoops ‘how many times you want to run your test’ ex: GLoops=2
4.	To execute from command line. using following command which run .jmx and excute test plan and create a .jtl file and create report dashboard .html: 
(Replace  what before /Jmeter with Jmeter bin folder path , before \ Takeaway.jmx , replace with .jmx directory) which is execution log that can be transferred using following command line : (Replace what before file.jtl with directory you want to save file into )to be viewed as .html apdex file including all needed details about execution “O:\Takeaway\apache-jmeter-5.2.1\apache-jmeter-5.2.1\bin\jmeter -GThreads=20 -GRampUP=20  -GLoops=20  -n -t O:\Takeaway\TestScript\Takeaway.jmx -l O:\Takeaway\Takeaway.jtl -e -o O:\Takeaway\Report 
Hint: please change file location path to yours at pervious command line 
# Execution and Guidelines:	
•	After installing Jmeter or using uploaded version there are 2 options
o	For Non GUI execution, after download or use upload version. Use following command and change directory path to yours over master machine:
“ O:\Takeaway\apache-jmeter-5.2.1\apache-jmeter-5.2.1\bin\jmeter -GThreads=20 -GRampUP=20  -GLoops=20  -n -t O:\Takeaway\TestScript\Takeaway.jmx -l O:\Takeaway\Takeaway.jtl -e -o O:\Takeaway\Report “ . Index.html file will be created .
Hint: Make sure Report folder is empty and .jtl is deleted before running to avoid any conflict.
o	First to run in GUI mode: by double clicking ApacheJmeter.jar as highlighted below:
o	 
o	Open Jmeter and choose .JMX file which located into TestScript folder
o	You will see the following: 
To start test, run green play button
o	 
o	Test Plan consist of thread groups, these thread group represent user scenario.
o	Thread group configuration is implemented using Jmeter property function ‘_P’ which takes a variable and value, default value is used if variable isn’t overwritten on runtime from command line
o	Every request sampler represent http request, URL, Port and encoding is provided from HTTP Request Default config. element. All Http protocols are supported.
o	2 Assertions made per API request which are: response code , items count or ID 
o	If any off assertions is failed , API sampler request is considered failure and logging assert failure message. However that doesn’t affect execution plan.
o	HTTP Header Manager is config. element for each http sampler defining (Referer, Accept-Language, Accept-Encoding, User-Agent, Accept ) 
o	Listeners used : view result tree to trace sampler http request ‘Header , body , raw request ‘ and response body/header/code. Aggregate Report: to get insights about number of sampler run , average response time , throughput , size …
o	Summary report is similar to aggregate report
o	Many other powerful listeners can be used even integration with elastic search but to avoid any problems when importing new plugins. Your Jmeter must download these plugins to open JMX so it abit tricky to configure although I already handled it over Dockerized solution
# Results
	If executed from Non-GUI mode , provided command will create index.html with all needed info.
	If executed for GUI , listeners would be enough to display needed info.

# Integration with CI/CD:
Hopefully environment used is Linux (Ubuntu) 
There are many ways available to integrate .jmx file with pipeline using Jenkins for example.
Simply point is as the following:
	Part related to environment installation, configuration and install even needed plugins is done and automated using Dockerized solution 
	This image consist of 3 files : Dockerfile (Image) , docker compose (Compose is a tool for defining and running multi-container Docker applications) , docker-entrypoint.sh to configure containers with some commands like startup bash commands such as allow remote access. As this dockerized image is designed for distributed execution specially performance.
	At pipeline if we take Jenkins for example : At pipeline Build action : there is an option to choose command shell . SO simply by sending needed commands you can achieve dreams ‘Power of bash’ so executing few commands whatever environment is, just:
At Docker folder, from command line if docker and docker compose is installed:
hostip=“machineIP” docker-compose up –d 
then this script will trigger Dockerfile image inside and build 1 master container , create test script folder over created container waiting to copy .jmx to it and overwrite TestData folder if exists 
	Through pipeline these execution commands can be run in sequence even ssh command over another server and execute docker commands , collect results and destroy containers and close servers or instances.

Please make sure to reach me to clarify any point.

